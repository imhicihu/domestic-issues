![stability-work_in_progress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #

* General issues found in the digital realm. By the way, a repo in _perpetual progress_
* This repo is a living document that will _grow_ and _adapt_ over and over again and...
![workflow.png](https://bitbucket.org/repo/ayrEdXL/images/525503978-workflow.png)

### What is this repository for? ###

* Quick summary
    - Technicalities, technical issues, proxies, scalabilities across different networks/operating systems/virtual scenarios, so... the _usual prospects_
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - There is no rule of thumb, so every issue demands knowledge and _mostly_ insightful thought
* Configuration
    - Just check it out the [`issues.md`](https://bitbucket.org/imhicihu/domestic-issues/src/master/issues.md)
* Dependencies
    - There is no current dependencies to deal, at least, in a general (digital) environment
* Database configuration
    - Just check it out our `snippets` section (up to now, for security reasons, _not_ visible to everybody)
![snippets.gif](https://bitbucket.org/repo/ayrEdXL/images/1270339170-snippets.gif)
* How to run tests
    - Every solution needs a particular test to manage, deal and pass it on

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/domestic-issues/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/domestic-issues/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/domestic-issues/commits/) section for the current status

### Contribution guidelines ###

* Writing tests
    - Follow our code convention
* Code review
    - Just PR your proposal/fixes/point of view _against_ this repo
* Other guidelines
    - Up to now, there is no other guidelines to verify/pass it on

### Related repositories ###

* Some repositories linked with this project:
     - [Proxy access](https://bitbucket.org/imhicihu/proxy-access/src/)
     - [Experimental (inner projects)](https://bitbucket.org/imhicihu/experimental-inner-projects/src/)
     - [Software installation](https://bitbucket.org/imhicihu/software-installations/src/)
     
### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/resources/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/domestic-issues/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)